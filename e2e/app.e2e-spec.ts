import { AulasAngularPage } from './app.po';

describe('aulas-angular App', () => {
  let page: AulasAngularPage;

  beforeEach(() => {
    page = new AulasAngularPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
